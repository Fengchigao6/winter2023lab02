public class MethodTest2 {
    public static void main(String[] args) {
       System.out.println(SecondClass.addOne(50));
	   SecondClass sc = new SecondClass();
	   System.out.println(sc.addTwo(50));
	   
    }
    public static void methodNoInputNoReturn(){
        System.out.println("I'm in a method that takes no input and returns nothing");
        int x = 20;
        System.out.println(x);
    }
    public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike){
        System.out.println("Inside the method one input no return");
		youCanCallThisWhateverYouLike -=5;
        System.out.println(youCanCallThisWhateverYouLike);
    }
	public static void methodTwoInputNoReturn(int value1, double value2){
		System.out.println(value1);
		System.out.println(value2);
	}
	public static int methodNoInputReturnInt(){
		return 5;
	}
	public static double sumSquareRoot(int value1, int value2){
		int value3 = value1 + value2;
		return Math.sqrt(value3);
	}
}