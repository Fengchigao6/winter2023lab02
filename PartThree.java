import java.util.Scanner;
public class PartThree {
    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);
        System.out.println("Please enter your first number");
        double firstNumber = scanner.nextDouble();
        System.out.println("Please enter your second number");
        double secondNumber =  scanner.nextDouble();
        double addResult = Calculator.add(firstNumber, secondNumber);
        double subtractResult =  Calculator.subtract(firstNumber, secondNumber);
        Calculator calculator = new Calculator();
        double multiplyResult = calculator.multiply(firstNumber, secondNumber);
        double divideResult = calculator.divide(firstNumber, secondNumber);
        System.out.println("Your additon result is "+ addResult);
        System.out.println("Your subtraction result is "+ subtractResult);
        System.out.println("Your multiplication result is "+ multiplyResult);
        System.out.println("Your division result is "+ divideResult);

    }
}